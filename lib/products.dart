import 'package:flutter/material.dart';

class Products extends StatelessWidget{
   final List <String> products;


  Products(this.products){
    print('[Product widget] Contructor');
  }

  @override
  Widget build(BuildContext context) {
    print('[Product widget] build()');
    return Column(
      children: products
          .map(
            (el) => Card(
                  elevation: 10.0,
                  margin: EdgeInsets.only(
                      top: 10.0, bottom: 10.0, left: 20.0, right: 20.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Image.asset('assets/food.jpg'),
                        margin: EdgeInsets.all(20.0),
                      ),
                      Container(
                        child: Text(el),
                        margin: EdgeInsets.only(bottom: 10.0),
                      ),
                    ],
                  ),
                ),
          )
          .toList(),
    );
  }
}
